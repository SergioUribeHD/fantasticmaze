using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class DeviceChecker : MonoBehaviour
{
#if UNITY_WEBGL && !UNITY_EDITOR
    [DllImport("__Internal")]
    public static extern bool IsMobile();

    private void Start()
    {
        Device.IsMobileOrTablet = IsMobile();
    }
#endif
}
