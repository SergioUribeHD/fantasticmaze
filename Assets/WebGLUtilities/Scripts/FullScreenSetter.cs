using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public class FullScreenSetter : MonoBehaviour
{
    [SerializeField] private Button fullscreenBtn = null;

#if UNITY_WEBGL && !UNITY_EDITOR
    [DllImport("__Internal")]
    public static extern void SetFullscreen(bool fullscreenMode);

#endif

    private void Awake()
    {
        fullscreenBtn.onClick.AddListener(EnterFullscreen);
    }

    private void Start()
    {
#if UNITY_WEBGL && !UNITY_EDITOR
        fullscreenBtn.gameObject.SetActive(true);
#else
        fullscreenBtn.gameObject.SetActive(false);
#endif
    }

    private void EnterFullscreen()
    {
#if UNITY_WEBGL && !UNITY_EDITOR
        SetFullscreen(true);
#endif
    }

}
