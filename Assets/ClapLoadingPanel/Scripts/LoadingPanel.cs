using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingPanel : MonoBehaviour
{
    [SerializeField] private Animator anim = null;
    [SerializeField] private AnimationClip fadeInClip = null;
    [SerializeField] private AnimationClip fadeOutClip = null;

    public bool IsLoopAnimationFinished { get; private set; }

    private static float originalTimeScale;

    public static bool Loaded { get; set; }

    public void FadeOut()
    {
        gameObject.SetActive(true);
        Loaded = false;
        anim.Play(fadeOutClip.name);
    }

    public IEnumerator CallFadeIn()
    {
        gameObject.SetActive(true);
        anim.Play(fadeInClip.name);
        yield return new WaitForSeconds(fadeInClip.length);
    }

    public void FreezeTimeScale()
    {
        originalTimeScale = Time.timeScale;
        Time.timeScale = 0f;
    }

    public void UnfreezeTimeScale()
    {
        Time.timeScale = originalTimeScale;
    }

    public void LoopAnimationFinishedTrue()
    {
        IsLoopAnimationFinished = true;
    }

    public void LoopAnimationFinishedFalse()
    {
        IsLoopAnimationFinished = false;
    }
    public void Disable()
    {
        UnfreezeTimeScale();
        gameObject.SetActive(false);
    }
}
