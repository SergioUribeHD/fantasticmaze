mergeInto(LibraryManager.library, {

    AlertLog: function (succeeded, logStr) {
        if (succeeded) {
            window.alert("Check console for details.");
        } else {
            window.alert("Error retrieving data");
        }
        console.log(logStr);
    },

    ReportCompleted: function (attemptJson, url) {
        window.parent.postMessage(Pointer_stringify(attemptJson), Pointer_stringify(url));
    },

    IsMobile: function()
    {
        return (/iPhone|iPad|iPod|Android/i.test(navigator.userAgent));
    },

    SetFullscreen: function(fullscreenMode) {
        if (fullscreenMode) {
            window.unityInstance.SetFullscreen(1);
        }
        else {
            window.unityInstance.SetFullscreen(0);
        }
    }

});