using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvincibilityAnimHandler : MonoBehaviour
{
    [SerializeField] private Invincibility invincibility = null;
    [SerializeField] private Animator anim = null;
    [SerializeField] private string booleanName = "IsInvincible";

    private void OnEnable()
    {
        Invincibility.OnStateChange += HandleStateChanged;
    }

    private void OnDisable()
    {
        Invincibility.OnStateChange -= HandleStateChanged;
    }

    private void HandleStateChanged(Invincibility invincibility)
    {
        if (this.invincibility != invincibility) return;
        anim.SetBool(booleanName, invincibility.IsInvincible);
    }
}
