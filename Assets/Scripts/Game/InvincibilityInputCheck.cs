using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvincibilityInputCheck : MonoBehaviour
{
    [SerializeField] private Invincibility invincibility = null;
    [SerializeField] private string buttonName = "Jump";

    private void OnEnable()
    {
        InputHandler.OnUpdate += HandleUpdate;
    }

    private void OnDisable()
    {
        InputHandler.OnUpdate -= HandleUpdate;
    }

    private void HandleUpdate()
    {
        if (!Input.GetButtonDown(buttonName)) return;
        invincibility.Use();
    }
}
