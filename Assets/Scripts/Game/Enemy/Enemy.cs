using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private PathPatrolling path = null;

    private bool isVulnerable;

    private void OnEnable()
    {
        Invincibility.OnStateChange += HandleInvincibilityRun;
    }

    private void OnDisable()
    {
        Invincibility.OnStateChange -= HandleInvincibilityRun;
    }

    private void HandleInvincibilityRun(Invincibility invincibility)
    {
        isVulnerable = invincibility.IsInvincible;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.TryGetComponent<Player>(out var player)) return;
        if (!isVulnerable)
            player.Die();
        else
            Die();
    }

    private void Die() => path.Restart();
}
