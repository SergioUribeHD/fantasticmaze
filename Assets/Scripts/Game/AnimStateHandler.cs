using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimStateHandler : MonoBehaviour
{
    [SerializeField] private Animator anim = null;
    [SerializeField] private string isWalkingBool = "Walking";
    [SerializeField] private Mover mover = null;

    private void Awake()
    {
        mover.OnMove += HandleMovement;
    }

    private void HandleMovement(Vector3 direction)
    {
        anim.SetBool(isWalkingBool, direction.normalized.magnitude > 0);
    }
}
