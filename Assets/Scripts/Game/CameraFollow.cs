﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private Transform target = null;
    [SerializeField] private float smoothSpeed = 10f;
    [SerializeField] private SpriteRenderer bkgRenderer = null;
    [SerializeField] private Vector3 offset = new Vector3(0f, 0f, -10f);

    private void Start()
    {
        transform.position = GetClampedPos(bkgRenderer, target.position + offset);
    }

    private void FixedUpdate()
    {
        Vector3 desiredPosition = target.position + offset;
        Vector3 smoothPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed * Time.deltaTime);
        transform.position = GetClampedPos(bkgRenderer, smoothPosition);
    }

    private void CalculateSpriteEdges(SpriteRenderer bkgRenderer, out Vector2 negativeEdge, out Vector2 positiveEdge)
    {
        Vector2 spriteUnits = bkgRenderer.sprite.rect.size / bkgRenderer.sprite.pixelsPerUnit * (Vector2)bkgRenderer.transform.lossyScale;
        Vector2 sprUnitsRot = Vector2.zero;
        if (bkgRenderer.transform.eulerAngles.z == 90f || bkgRenderer.transform.eulerAngles.z == 270)
        {
            float sprUnitsX = spriteUnits.x;
            spriteUnits.x = spriteUnits.y;
            spriteUnits.y = sprUnitsX;
        }
        negativeEdge = (Vector2)bkgRenderer.transform.position - (spriteUnits / 2);
        positiveEdge = (Vector2)bkgRenderer.transform.position + (spriteUnits / 2);
    }

    private Vector2 GetCameraUnits()
    {
        var cam = Camera.main;
        float aspectRatioFactor = (float)Screen.width / Screen.height;
        Vector2 cameraUnits = new Vector2(cam.orthographicSize * aspectRatioFactor, cam.orthographicSize);
        return cameraUnits;
    }

    private Vector3 GetClampedPos(SpriteRenderer bkgRenderer, Vector3 smoothPosition)
    {
        CalculateSpriteEdges(bkgRenderer, out var bkgNegativeEdge, out var bkgPositiveEdge);
        Vector2 camUnits = GetCameraUnits();
        float clampPosX = Mathf.Clamp(smoothPosition.x, bkgNegativeEdge.x + camUnits.x, bkgPositiveEdge.x - camUnits.x);
        float clampPosY = Mathf.Clamp(smoothPosition.y, bkgNegativeEdge.y + camUnits.y, bkgPositiveEdge.y - camUnits.y);
        return new Vector3(clampPosX, clampPosY, smoothPosition.z);
    }
}
