using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Path : MonoBehaviour
{
    [SerializeField] private bool invertOnEndReached = false;
    private int currentTargetWaypointIndex;

    private bool EndReached => currentTargetWaypointIndex == transform.childCount - 1;
    private bool BegginingReached => currentTargetWaypointIndex == 0;

    private int direction;

    public Vector2 TargetWaypoint => transform.GetChild(currentTargetWaypointIndex).position;

    private void Start()
    {
        RestartTarget();
    }

    public void CheckTargetWaypointReached(Vector2 currentPos)
    {
        if (Vector2.Distance(currentPos, TargetWaypoint) > 0.01f) return;
        UpdateTargetWaypoint();
    }

    private void UpdateTargetWaypoint()
    {
        currentTargetWaypointIndex += direction;
        if (EndReached && !invertOnEndReached)
            currentTargetWaypointIndex = 0;
        else if ((EndReached || BegginingReached) && invertOnEndReached)
            direction *= -1;
    }

    public void RestartTarget()
    {
        direction = 1;
        currentTargetWaypointIndex = 0;
    }

}
