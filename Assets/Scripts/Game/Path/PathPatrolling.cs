using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathPatrolling : MonoBehaviour
{
    [SerializeField] private Path path = null;
    [SerializeField] private Mover mover = null;

    private void Start()
    {
        transform.position = path.TargetWaypoint;
    }

    private void FixedUpdate()
    {
        Vector2 targetPos = (path.TargetWaypoint - (Vector2)transform.position).normalized;
        mover.Move(targetPos, Time.fixedDeltaTime);
        path.CheckTargetWaypointReached(transform.position);
    }

    public void Restart()
    {
        path.RestartTarget();
        transform.position = path.TargetWaypoint;
    }


}
