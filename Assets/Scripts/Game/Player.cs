using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private Transform startingPoint = null;

    


    private void Start()
    {
        transform.position = startingPoint.position;
    }

    public void Die()
    {
        transform.position = startingPoint.position;
    }

}
