using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour
{
    [field: SerializeField] public float MoveSpeed { get; set; } = 7f;


    public event Action<Vector3> OnMove = null;

    public bool Enabled { get; set; } = true;


    private Vector3 originalScale;

    private void Start()
    {
        originalScale = transform.localScale;
    }

    public void Move(Vector3 direction, float deltaTime)
    {
        OnMove?.Invoke(direction);
        if (!Enabled)
            direction = Vector3.zero;
        transform.position += direction * MoveSpeed * deltaTime;
        if (direction.x == 0) return;
        Vector3 scale = originalScale;
        scale.x = originalScale.x * (direction.x > 0 ? 1 : -1);
        transform.localScale = scale;
    }

}
