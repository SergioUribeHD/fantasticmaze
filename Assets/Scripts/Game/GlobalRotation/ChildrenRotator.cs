using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChildrenRotator : MonoBehaviour
{
    private void OnEnable()
    {
        ParentRotator.OnRotation += HandleParentRotation;
    }

    private void OnDisable()
    {
        ParentRotator.OnRotation -= HandleParentRotation;
    }

    private void HandleParentRotation(Transform parent)
    {
        if (transform.parent != parent) return;
        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, parent.localEulerAngles.z * -1);
    }
}
