using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParentRotator : MonoBehaviour
{
    public static event Action<Transform> OnRotation;

    public void Rotate(float degrees)
    {
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, degrees);
        OnRotation?.Invoke(transform);
    }

    
}
