using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParentRotator))]

public class ParentRotatorFuncTest : MonoBehaviour
{
    private ParentRotator Rotator => GetComponent<ParentRotator>();

    private IEnumerator Start()
    {
        yield return new WaitForSeconds(10f);
        Rotator.Rotate(90f);
        yield return new WaitForSeconds(10f);
        Rotator.Rotate(180f);
        yield return new WaitForSeconds(10f);
        Rotator.Rotate(270f);
        yield return new WaitForSeconds(10f);
        Rotator.Rotate(0f);
        yield return new WaitForSeconds(10f);
        Rotator.Rotate(180f);
    }
}
