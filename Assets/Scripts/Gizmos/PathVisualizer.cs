using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathVisualizer : MonoBehaviour
{
    [SerializeField] private Color color = Color.green;
    private void OnDrawGizmos()
    {
        if (transform.childCount == 0) return;
        for (int i = 0; i < transform.childCount; i++)
        {
            Gizmos.color = color;
            Gizmos.DrawSphere(transform.GetChild(i).position, 0.2f);
            if (i == 0) continue;
            Gizmos.DrawLine(transform.GetChild(i - 1).position, transform.GetChild(i).position);
        }
        
    }
}
