using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InvincibilityUsesPresenter : MonoBehaviour
{
    [SerializeField] private Invincibility invincibility = null;
    [SerializeField] private Button button = null;
    [SerializeField] private Text usesText = null;
    [SerializeField] private Image timerImg = null;

    private void Awake()
    {
        button.onClick.AddListener(invincibility.Use);
        invincibility.OnTimeUpdated += HandleTimeUpdated;
        invincibility.OnRemainingUsesUpdated += HandleRemainingUsesUpdated;
    }

    private void OnEnable()
    {
        InputHandler.OnUpdate += HandleUpdate;
    }

    private void OnDisable()
    {
        InputHandler.OnUpdate -= HandleUpdate;
    }

    private void Start()
    {
        HandleRemainingUsesUpdated();
        HandleTimeUpdated(0f);
    }


    private void HandleUpdate()
    {
        button.interactable = invincibility.CanUse;
    }

    private void HandleTimeUpdated(float timePercentage)
    {
        timerImg.fillAmount = timePercentage;
    }

    private void HandleRemainingUsesUpdated()
    {
        usesText.text = invincibility.RemainingUses.ToString();
    }
}
