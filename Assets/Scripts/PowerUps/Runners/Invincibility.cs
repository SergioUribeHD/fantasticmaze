using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Invincibility : MonoBehaviour, IPowerup, ITimer
{
    [SerializeField] private int maxUseCapacity = 3;
    [SerializeField] private float time = 10f;
    public bool IsInvincible { get; private set; }

    public bool CanUse => !IsInvincible && RemainingUses > 0;

    public static event Action<Invincibility> OnStateChange = null;
    public event Action OnRemainingUsesUpdated = null;
    public event Action<float> OnTimeUpdated;

    public int RemainingUses
    {
        get => remainingUses;
        set
        {
            remainingUses = Mathf.Clamp(value, 0, maxUseCapacity);
            OnRemainingUsesUpdated?.Invoke();
        }
    }

    private int remainingUses;

    private float currentTime;

    private Coroutine invincible;

    public void Pick()
    {
        RemainingUses++;
    }

    public void Use()
    {
        if (!CanUse) return;
        if (invincible != null)
            StopCoroutine(invincible);
        RemainingUses--;
        invincible = StartCoroutine(MakeInvincible());
    }

    private IEnumerator MakeInvincible()
    {
        currentTime = time;
        IsInvincible = true;
        OnStateChange?.Invoke(this);
        while (currentTime > 0f)
        {
            yield return new WaitForEndOfFrame();
            currentTime = Mathf.Max(0f, currentTime - Time.deltaTime);
            OnTimeUpdated?.Invoke(currentTime / time);
        }
        IsInvincible = false;
        OnStateChange?.Invoke(this);
    }

}
