using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPowerup
{
    void Pick();

    void Use();
}
