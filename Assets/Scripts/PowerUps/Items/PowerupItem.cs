using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupItem<T> : MonoBehaviour where T : IPowerup
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.TryGetComponent<IPowerup>(out var powerupRunner)) return;
        powerupRunner.Pick();
        gameObject.SetActive(false);
    }

}
