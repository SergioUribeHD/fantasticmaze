using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITimer
{
    public event Action<float> OnTimeUpdated;
}
