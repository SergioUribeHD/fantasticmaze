using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    public static event Action OnUpdate = null;

    void Update()
    {
        OnUpdate?.Invoke();
    }
}
