using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(InputHandler))]
public class MovementInputHandler : MonoBehaviour
{
    [SerializeField] private GameObject mobileControlsUI = null;
    [SerializeField] private Joystick joystick = null;
    [field: SerializeField] public Mover Mover { get; set; }

    private Vector3 movement;

    public bool Enabled { get; set; } = true;


    private void OnEnable()
    {
        mobileControlsUI.SetActive(Device.IsMobileOrTablet);
        InputHandler.OnUpdate += HandleUpdate;
    }

    private void OnDisable()
    {
        InputHandler.OnUpdate -= HandleUpdate;
    }

    private void HandleUpdate()
    {
        if (Device.IsMobileOrTablet)
        {
            movement.x = Enabled ? joystick.Horizontal : 0;
            movement.y = Enabled ? joystick.Vertical : 0;
        }
        else
        {
            movement.x = Enabled ? Input.GetAxisRaw("Horizontal") : 0;
            movement.y = Enabled ? Input.GetAxisRaw("Vertical") : 0;
        }
    }


    private void FixedUpdate()
    {
        if (Mover == null) return;
        Mover.Move(movement, Time.fixedDeltaTime);
    }
}
